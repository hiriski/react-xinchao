export const ROUTES = {
  SIGNIN: '/signin',
  SIGNUP: '/signup',
  LOGGEDOUT: '/logged-out',
  PHRASEBOOK_LIST: '/phrasebook',
};
