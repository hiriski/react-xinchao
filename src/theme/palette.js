import { colors } from '@material-ui/core';

export default {
  background: {
    dark: '#F4F6F8',
    default: '#fbfbfb',
    paper: colors.common.white,
  },
  primary: {
    light: '#ffc6d0',
    main: '#FF4564',
    dark: '#f10930',
    contrastText: '#fff',
  },
  secondary: {
    main: '#28272f',
    dark: '#111113',
    light: '#111113',
  },
  text: {
    primary: colors.blueGrey[900],
    secondary: colors.blueGrey[600],
  },
};
